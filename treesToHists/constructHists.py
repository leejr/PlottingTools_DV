#!/usr/bin/env python

from ROOT import *
from array import array
import ROOT

ROOT.gROOT.LoadMacro("./AtlasStyle.C")
ROOT.gROOT.LoadMacro("~/atlasstyle-00-03-04/AtlasLabels.C")
ROOT.SetAtlasStyle()
# from ATLASStyle import *



f = TFile("data.root")
t = f.Get("DVTree_NTuple")

outputFile = TFile("hists.root","RECREATE")


nBinsR = 300
nBinsPhi = 100

binLowR = [300./nBinsR*x for x in xrange(nBinsR+1)]
binLowPhi = [2*TMath.Pi()/nBinsPhi*x for x in xrange(nBinsPhi+1)]
# binLowZ = [-300,-250,-200,-150,-125,-100,-75,-50,-25,0,25,50,75,100,125,150,200,250,300]
# binLowZ = [-300 + x*600/50. for x in xrange(50)]

nbins = 12
binLowZPos = [0 + x*100/nbins for x in xrange(1,nbins+1)]
nbins = 8
binLowZPos = binLowZPos + [binLowZPos[-1] + x*100/nbins for x in xrange(1,nbins+1)]
nbins = 2
binLowZPos = binLowZPos + [binLowZPos[-1] + x*50/nbins for x in xrange(1,nbins+1)]
nbins = 1
binLowZPos = binLowZPos + [binLowZPos[-1] + x*50/nbins for x in xrange(1,nbins+1)]

binLowZNeg = [-1.*x for x in binLowZPos]
binLowZNeg.reverse()

binLowZ = binLowZNeg + [0.] + binLowZPos

#print binLowZPos

print binLowZ
print binLowR
print binLowPhi
print binLowZ

outputMap = TH3D("outputMap","",\
	len(binLowR)-1  ,array('d',binLowR),\
	len(binLowPhi)-1,array('d',binLowPhi),\
	len(binLowZ)-1  ,array('d',binLowZ),\
	)


# t.SetBranchStatus("*",0)
# t.SetBranchStatus("DV_*",1)

DV_phi = "TVector2::Phi_0_2pi(TMath::ATan2(DV_y,DV_x) )"

# "*(1e6+(5e3/2)*abs(DV_z))"

# DV_rxyz = "sqrt(DV_x*DV_x+DV_y*DV_y+DV_z*DV_z)"
cuts = "(DV_chisqPerDOF<5)*((DV_m < 0.5 - DV_R*0.0015)||(DV_m > 0.5 + DV_R*0.0015))"
# # weightstring = cuts+"*pow(" + DV_rxyz +",1)*DV_R"
weightstring = cuts+"*pow(DV_R,0.3)"



t.Draw("DV_z:%s:DV_R>>outputMap"%DV_phi,weightstring)
outputMap.Write()

outputMap.Project3D("x")
outputMap.Project3D("y")
outputMap.Project3D("z")

h_yx = outputMap.Project3D("yx")
h_yz = outputMap.Project3D("yz")
h_xz = outputMap.Project3D("xz")

c = TCanvas("c","c",600,600)

h_yx.Draw("colz")
h_yx.GetXaxis().SetTitle("Vertex r_{xy} [mm]")
h_yx.GetYaxis().SetTitle("Vertex #phi")
ATLASLabel(0.2,0.9,"Internal  "   )
c.SaveAs("vertex_yx.pdf")
h_yx.GetXaxis().SetRangeUser(0,70)
c.SaveAs("vertex_yx_Zoom.pdf")


h_xz.Draw("colz")
h_xz.GetXaxis().SetTitle("Vertex r_{xy} [mm]")
h_xz.GetYaxis().SetTitle("Vertex z [mm]")
ATLASLabel(0.2,0.9,"Internal  "   )
c.SaveAs("vertex_xz.pdf")


outputFile.Write()
outputFile.Close()

