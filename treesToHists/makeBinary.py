#!/usr/bin/env python

from ROOT import *
import ROOT

ROOT.gROOT.LoadMacro("./AtlasStyle.C")
ROOT.gROOT.LoadMacro("~/atlasstyle-00-03-04/AtlasLabels.C")
ROOT.SetAtlasStyle()
# from ATLASStyle import *


# f = TFile("hists_highstats.root")
f = TFile("materialMap3D_Run2_v2.root")
h = f.Get("map")

outputFile = TFile("map.root","RECREATE")


outputBinaryMap = h.Clone("map")
outputBinaryMap.Reset()

cleanUp = True

isAlreadyBinary = True

for ibin in xrange(int( (outputBinaryMap.GetNbinsX()+1)*(outputBinaryMap.GetNbinsY()+1)*(outputBinaryMap.GetNbinsZ()+1) *1.2) ):

	if ibin%10000==0:
		print ibin

	outputBinaryMap.SetBinContent(ibin,0)


	binx,biny,binz = Long(),Long(),Long()
	h.GetBinXYZ(ibin, binx, biny, binz  )

	xcenter = h.GetXaxis().GetBinCenter(binx);
	ycenter = h.GetYaxis().GetBinCenter(biny);
	zcenter = h.GetZaxis().GetBinCenter(binz);

	# if zcenter < 250:
	# 	continue

	if abs(zcenter)>350:
		continue

	phi = ycenter
	z = zcenter
	r = xcenter

	x = r*TMath.Cos(phi)
	y = r*TMath.Sin(phi)

	# print h.GetBinContent(ibin)

	threshold = 250
	if r < 20:
		threshold = 1e9999
	# elif r > 210:
	# 	threshold = 1e9999

	if -50 < z < 50:
		threshold = threshold*1.2
	if -30 < z < 30:
		threshold = threshold*1.2
	if -10 < z < 10:
		threshold = threshold*1.2

	if 200 < abs(z) < 250:
		threshold = threshold*1.2
	elif 250 < abs(z):
		threshold = threshold*1.2

	if r < 30:
		threshold = threshold * 2
	elif r < 40:
		threshold = threshold * 1.2
	elif r < 46:
		threshold = threshold * 5
	elif r < 55:
		threshold = threshold * 0.9
	elif r < 62:
		threshold = threshold * 0.4 #0.5
	elif r < 67:
		threshold = threshold * 0.25
		if abs(z)<100:
			threshold = threshold * 0.75
	elif r < 83:
		threshold = threshold * 2.
	elif r < 93:
		threshold = threshold * 0.4
	elif r < 97:
		threshold = threshold * 0.1
	elif r < 100:
		threshold = threshold * 0.25
	elif r < 106:
		threshold = threshold * 0.05
		if abs(z)<100:
			threshold = threshold * 0.75
	elif r < 119:
		threshold = threshold * 1.4
	elif r < 127:
		threshold = threshold * 0.85
		if abs(z)<100:
			threshold = threshold * 0.75
	elif r < 132:
		threshold = threshold * 0.2
		if abs(z)<100:
			threshold = threshold * 0.75
		if abs(z)<50:
			threshold = threshold * 0.75

	elif r < 135:
		threshold = threshold * 1.0
	elif r < 141:
		threshold = threshold * 0.3
		if abs(z)<100:
			threshold = threshold * 0.75
		if abs(z)<50:
			threshold = threshold * 0.75


	if h.GetBinContent(ibin)>threshold:
		outputBinaryMap.SetBinContent(ibin,1.)

	doHardcodedGeo = True
	newHardcodedGeo = True

	#hardcoded geo
	if doHardcodedGeo:
		hardcodedVeto = False

		if not newHardcodedGeo:
			if (r >= 20.0 and r <= 26.5):
				hardcodedVeto = True
			if (r >= 42.5 and r <= 44.5) :
				hardcodedVeto = True

			if ((r >= 64 and r <= 73) and ((z >= -220 and z <= -195) or (z >= -25 and z <= -10) or (z >= 223 and z <= 238))):
				hardcodedVeto = True
			elif ((r >= 69 and r <= 73) and not ((z >= -220 and z <= -195) or (z >= -25 and z <= -10) or (z >= 223 and z <= 238))):
				hardcodedVeto = True

		if ((r >= 102 and r <= 111) and ((z >= -220 and z <= -195) or (z >= -25 and z <= -10) or (z >= 223 and z <= 238))):
			hardcodedVeto = True
		elif ((r >= 107 and r <= 111) and not ((z >= -220 and z <= -195) or (z >= -25 and z <= -10) or (z >= 223 and z <= 238))):
			hardcodedVeto = True
		elif ((r >= 136 and r <= 145) and ((z >= -220 and z <= -195) or (z >= -25 and z <= -10) or (z >= 223 and z <= 238))):
			hardcodedVeto = True
		elif ((r >= 141 and r <= 145) and not ((z >= -220 and z <= -195) or (z >= -25 and z <= -10) or (z >= 223 and z <= 238))):
			hardcodedVeto = True
		elif ((r >= 225 and r <= 240) and ((z >= -300 and z <= -240) or (z >= -30 and z <= 30) or (z >= 240 and z <= 300))):
			hardcodedVeto = True
		elif ((r >= 226 and r <= 231) and not ((z >= -300 and z <= -240) or (z >= -30 and z <= 30) or (z >= 240 and z <= 300))):
			hardcodedVeto = True

		elif (r >= 255 and r <= 261):
			hardcodedVeto = True

#		elif ((r >= 262 and r <= 277) and ((z >= -250 and z <= -240) or (z >= 240 and z <= 250)) and ((phi % 0.7854) >= 0.34 and (phi % 0.7854) <= 0.46)):
		elif ((r >= 262 and r <= 277) and ((z <= -240) or (z >= 240)) and ((phi % 0.7854) >= 0.34 and (phi % 0.7854) <= 0.46)):
			hardcodedVeto = True

		elif (r >= 277 and r <= 300):
			hardcodedVeto = True

		if 215 < r < 225:
			if 5.33 < phi < 5.58:
				hardcodedVeto = True
			if 3.86 < phi < 4.03:
				hardcodedVeto = True

		pi = TMath.Pi()
		n = 8 #octagon
		distToOctagon=195*1./TMath.Cos(phi%(2*pi/n)-pi/n)
		# distToOctagon=210*TMath.Cos(pi/n)/TMath.Cos((n*phi)%1/n-pi/n);
		# print distToOctagon
		if distToOctagon-12 < r < distToOctagon+12:
			# print "octagon!"
			hardcodedVeto = True


		if newHardcodedGeo:
			distToCircle=TMath.Sqrt(x**2+(y+1.5)**2)
			if abs(distToCircle - 24) < 1.5:
				hardcodedVeto = True
			distToCircle=TMath.Sqrt((x+0.3)**2+(y+0.5)**2)
			if abs(distToCircle - 29) < 0.8:
				hardcodedVeto = True
			distToCircle=TMath.Sqrt((x+0.2)**2+(y+1)**2)
			if abs(distToCircle - 42.5) < 1.2:
				hardcodedVeto = True

			distToCircle=TMath.Sqrt((x+0.1)**2+(y+0.5)**2)
			## edited given binning of map
			# if ((z >= -210 and z <= -195) or (z >= -25 and z <= -10) or (z >= 223 and z <= 238)):
			if ((z >= -210 and z <= -187) or (z >= -25 and z <= -10) or (z >= 223 and z <= 250)):
				if 64 < distToCircle < 73:
					hardcodedVeto = True
			else:
				if 70 < distToCircle < 73:
					hardcodedVeto = True



		if hardcodedVeto:
			outputBinaryMap.SetBinContent(ibin,1.)


# for ibin in xrange((outputBinaryMap.GetNbinsX()+1)*(outputBinaryMap.GetNbinsY()+1)*(outputBinaryMap.GetNbinsZ()+1) ):
# 	if ibin%1000==0:
# 		print ibin
# 		print outputBinaryMap.GetBinContent(ibin)

# 		# print 	outputBinaryMap.GetBinContent(ibin)


if cleanUp:
	print "doing cleanUp"
	for ibin in xrange(
		int ((outputBinaryMap.GetNbinsX()+1)*(outputBinaryMap.GetNbinsY()+1)*(outputBinaryMap.GetNbinsZ()+1)*1.2)  ):
		if ibin%10000==0:
			print ibin
			# print outputBinaryMap.GetBinContent(ibin)
		if outputBinaryMap.GetBinContent(ibin)==0:
			binx,biny,binz = Long(),Long(),Long()
			outputBinaryMap.GetBinXYZ(ibin, binx, biny, binz  )
			nBinsOfOne = outputBinaryMap.GetBinContent(outputBinaryMap.GetBin(binx-1,biny,binz))
			nBinsOfOne += outputBinaryMap.GetBinContent(outputBinaryMap.GetBin(binx+1,biny,binz))
			nBinsOfOne += outputBinaryMap.GetBinContent(outputBinaryMap.GetBin(binx,biny-1,binz))
			nBinsOfOne += outputBinaryMap.GetBinContent(outputBinaryMap.GetBin(binx,biny+1,binz))
			nBinsOfOne += outputBinaryMap.GetBinContent(outputBinaryMap.GetBin(binx,biny,binz-1))
			nBinsOfOne += outputBinaryMap.GetBinContent(outputBinaryMap.GetBin(binx,biny,binz+1))
			if nBinsOfOne>3:
				# print nBinsOfOne
				# print "masking from cleanUp"
				outputBinaryMap.SetBinContent(ibin,1.0)


outputMapRebinned1 = outputBinaryMap.Clone("map")
# outputMapRebinned2 = outputBinaryMap.Clone("map_rebinned_2")
# outputMapRebinned4 = outputBinaryMap.Clone("map_rebinned_4")
# outputMapRebinned2.Rebin3D(2,2,1)
# outputMapRebinned4.Rebin3D(4,4,1)


c = TCanvas("c","c",600,600)

h_yx = outputBinaryMap.Project3D("yx")
h_yz = outputBinaryMap.Project3D("yz")
h_xz = outputBinaryMap.Project3D("xz")

h_yx.Draw("colz")
h_yx.GetXaxis().SetTitle("Vertex r_{xy} [mm]")
h_yx.GetYaxis().SetTitle("Vertex #phi")
ATLASLabel(0.2,0.9,"Internal  "   )
c.SaveAs("map_yx.pdf")
h_yx.GetXaxis().SetRangeUser(0,70)
c.SaveAs("map_yx_Zoom.pdf")


h_xz.Draw("colz")
h_xz.GetXaxis().SetTitle("Vertex r_{xy} [mm]")
h_xz.GetYaxis().SetTitle("Vertex z [mm]")
ATLASLabel(0.2,0.9,"Internal  "   )
c.SaveAs("map_xz.pdf")


outputFile.Write()
outputFile.Close()

